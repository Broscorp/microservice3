package com.acme.microservice3.filter;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class HostnameUtil {

    public static String getHostname() {
        String hostname = null;
        try {
            InetAddress ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
        } catch (UnknownHostException e) {
            hostname = "N/A";
        }
        return hostname;
    }
}
