package com.acme.microservice3.filter;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
public class UuidResponseAdvice implements ResponseBodyAdvice {

  private static final String HEADER_VALUE = "ms3-" + HostnameUtil.getHostname();
  private static final String MS_HEADER_NAME = "X-Microservice-Flow";

  @Override
  public boolean supports(MethodParameter returnType, Class converterType) {
    return true;
  }

  @Override
  public Object beforeBodyWrite(Object body, MethodParameter returnType,
      MediaType selectedContentType,
      Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
    response.getHeaders().add(MS_HEADER_NAME, HEADER_VALUE);
    return body;
  }
}
