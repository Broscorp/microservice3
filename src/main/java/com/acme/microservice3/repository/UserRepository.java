package com.acme.microservice3.repository;

import com.acme.microservice3.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
