package com.acme.microservice3.controller;

import com.acme.microservice3.model.User;
import com.acme.microservice3.repository.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = {"/users"})
public class UserController {

  private final UserRepository userRepository;

  public UserController(UserRepository userService) {
    this.userRepository = userService;
  }

  @PostMapping
  public User saveUser(@RequestBody User user) {
    user.setId(userRepository.save(user).getId());
    return user;
  }

  @GetMapping
  public Iterable<User> findAll() {
    return userRepository.findAll();
  }

  @GetMapping("/{id}")
  public User findById(@PathVariable long id) {
    return userRepository.findById(id).orElse(null);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<Void> deleteById(@PathVariable long id) {
    userRepository.deleteById(id);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
